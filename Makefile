all: LilithCarpenter.pdf

.PHONY: LilithCarpenter.pdf

LilithCarpenter.pdf:
	xelatex LilithCarpenter.tex
	xelatex LilithCarpenter.tex

clean:
	-rm -fr *.aux
	-rm -fr *.log
	-rm -fr *.nav
	-rm -fr *.out
	-rm -fr *.snm
	-rm -fr *.toc
	-rm -fr *.pdf
	-rm -fr *.vrb

.PHONY: clean all
